# reliable-ui

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Alternatively run a build

```
Serve files in /dist
```
### Customize configuration
```
Edit VUE_APP_API_ENDPOINT property in .env file
```